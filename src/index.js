import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import * as serviceWorker from "./serviceWorker";
import HeaderTop from "./Component/HeaderTop/HeaderTop";
import Header from "./Component/Header/Header";
import VideoBody from "./Component/VideoBody/VideoBody";
import ImageBody from "./Component/ImageBody/ImageBody";
import BannerWeb from "./Component/BannerWeb/BannerWeb";
import ProductImage from "./Component/ProductImage/ProductImage";
import JustWatch from "./Component/JustWatch/JustWatch";
import LastText from "./Component/LastText/LastText";
import Footer from "./Component/Footer/Footer";
import "bootstrap/dist/css/bootstrap.min.css";
import { createStore } from "redux";
import { Provider } from "react-redux";

//reducer
const globalState = {
  value: 0,
  price: 0,
  show: false,
  addToCart: false,
  detailProduct: false
};

const rootReducer = (state = globalState, action) => {
  if (action.type === "Plus") {

    return {
      ...state,
      value: state.value + 1,
      price: (state.value + 1) * 2450000
    };
  }

  if (action.type === "Minus") {
    if (state.value > 0) {
      return {
        ...state,
        value: state.value - 1,
        price: (state.value - 1) * 2450000
      };
    }
  }

  if(action.type === "Open"){
      return {
        ...state,
        show: true
      }
  }

  if(action.type === "Close"){
    return {
      ...state,
      show: false
    }
  }
  if(action.type === "addToCart"){
    return {
      ...state,
      addToCart: true,
    }
  }


  return state;
};

//store
const store = createStore(rootReducer);

class App extends React.Component {
  render() {
    return (
      <React.Fragment>
        <Provider store={store}>
          <HeaderTop />
          <Header />
          <VideoBody />
          <ImageBody />
          <BannerWeb />
          <ProductImage />
          <JustWatch />
          <LastText />
          <Footer />
        </Provider>
      </React.Fragment>
    );
  }
}

ReactDOM.render(<App />, document.getElementById("root"));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
