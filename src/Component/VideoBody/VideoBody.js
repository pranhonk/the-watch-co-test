import React,{Fragment} from 'react';
import './Q-Timex_TW2T80700_720_70sec WEB.mp4'

const sources = {
    QTimex : "https://img.youtube.com/vi/TA9s_S-0C_Q/maxresdefault.jpg"
}

class VideoBody extends React.Component{
    constructor(props){
        super(props);

        this.state = {
            source: sources.QTimex
        }
    }
    render(){
        return(
            <Fragment>
                <img src={this.state.source} alt="Qtimex" className={"w-100 h-auto"}/>
            </Fragment>
        );
    }
}

export default VideoBody;