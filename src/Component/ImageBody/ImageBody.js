import React, {Fragment} from 'react';
import "./ImageBody.css"
import {Container, Row, Col} from 'react-bootstrap';
import bg from '../Assets/Image/Desktop banner.jpg';
import bpPhone from '../Assets/Image/mobile banner.jpg';


function ImageBody() {
return(
    <Fragment>
        <section className="bg">
            <img src={bg} alt="this image" className="bgdesktop"/>
            <img src={bpPhone} alt="phone image" className="bgphone"/>
            <Container>
                <Row>
                    <Col sm={6}></Col>
                    <Col sm={6}>
                        <div className="centering">
                            <h1>Q Timex</h1>
                            <h4>1979 Reissue</h4>
                            <p className="mt-4">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda deleniti perferendis ut? Cupiditate doloribus eaque enim iure officiis quisquam sunt, vero. Doloremque eligendi esse ipsa iusto pariatur recusandae vitae voluptas!
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab aut consequuntur ea fuga ipsum non praesentium qui quidem quis, reprehenderit soluta vel vitae? Earum impedit, incidunt ipsum recusandae tempora vitae?</p>
                        </div>
                    </Col>
                </Row>
            </Container>
        </section>
    </Fragment>
    )
}

export default ImageBody;