import React, {Fragment} from 'react';
import NumberFormat from 'react-number-format';
import {Button} from 'react-bootstrap';
import watch from '../Assets/Image/P7.png';
import {connect} from 'react-redux';

class Divider extends React.Component {
    render() {
        let title = this.props.title;
        let desc = this.props.desc;
        return (
            <Fragment>
                <img src={watch} alt="Watches" className="w-100 h-auto"/>
                <h5 className="mt-2">{title}</h5>
                <p className="font-weight-bold mt-3 mb-0">Description</p>
                <p className="mt-1">{desc}</p>
                <React.Fragment>
                    <hr/>
                    <div className="d-inline-block my-3">
                        <Button onClick={this.props.Minus} variant="dark" className="d-inline mr-2">-</Button>
                        <input type="number" value={this.props.total}/>
                        <Button onClick={this.props.Plus} variant="dark" className="d-inline ml-2">+</Button>
                    </div>
                    <div className="d-block">
                        <h5>Total:</h5>
                        <NumberFormat value={this.props.price} className="text-success font-weight-bold"
                                      displayType={'Number'} thousandSeparator={true} prefix={'IDR'}/><br/>
                        <Button variant="success" className="mt-2" onClick={this.props.addToCart}
                                disabled={this.props.total === 0}>Add To Cart</Button>
                    </div>
                </React.Fragment>
            </Fragment>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        total: state.value,
        price: state.price
    }
};

const mapDispatchToProps = (dispacth) => {
    return {
        Plus: () => dispacth({type: 'Plus'}),
        Minus: () => dispacth({type: 'Minus'}),
        addToCart: () => dispacth({type: 'addToCart'})

    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Divider);