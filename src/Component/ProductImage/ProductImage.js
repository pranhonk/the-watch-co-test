import React from 'react';
import {Container, Row, Col, ButtonToolbar} from 'react-bootstrap';
import FirstImage from '../Assets/Image/Visual Mobile Desktop-02.jpg'
import watch from '../Assets/Image/P7.png';
import './PorductImage.css';
import Example from './Modal';




function ProductImage() {
    return (
        <React.Fragment>
            <Container>
                <div className="my-5">
                    <Row>
                        <Col xl={6}>
                            <img src={FirstImage} alt="Mobile-Desktop" className={"w-100 h-auto"}/>
                        </Col>
                        <Col xl={6}>
                            <img src={watch} alt="Q Timex Watch" className="w-100 h-auto"/>
                            <div className="text-center">
                                <h5 className="text-uppercase mt-4">Q timex 1979 reissue</h5>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium eligendi est
                                    ipsam molestiae non placeat. Culpa dolor eligendi facilis magni nostrum omnis
                                    possimus praesentium quibusdam quo, sequi sint vel voluptates!</p>
                                <div className="price mt-4">
                                    <h5 className="text-uppercase">idr 2.450.000</h5>
                                    <p className="font-weight-light">IDR 162.500/bulan</p>
                                </div>
                                <ButtonToolbar className="justify-content-center mt-5">
                                    <Example name="Specs" class="buttonPrimary"/>
                                </ButtonToolbar>
                            </div>
                        </Col>
                    </Row>
                </div>
            </Container>
        </React.Fragment>
    )
}


export default ProductImage;

