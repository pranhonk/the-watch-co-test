import React, {useState}from 'react';
import Divider from './Divider';
import {Button, Modal} from 'react-bootstrap';


function Example(props) {
    const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    return (
        <>
            <Button variant="primary" className={props.class} onClick={handleShow}>
                {props.name}
            </Button>

            <Modal show={show} onHide={handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>Q timex 1979 reissue</Modal.Title>
                </Modal.Header>
                <Modal.Body><Divider title="Q timex 1979 reissue"
                                     desc="Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid cumque dolor fugit, illum iusto neque nihil nisi non numquam perspiciatis possimus, praesentium quae recusandae repudiandae saepe! Impedit laudantium reprehenderit soluta."
                                     price="IDR 500.000"/></Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>
                        Close
                    </Button>
                </Modal.Footer>
            </Modal>
        </>
    );
}

export default Example