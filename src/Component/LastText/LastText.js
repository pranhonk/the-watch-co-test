import React,{useState} from 'react';
import {Container, Row, Col, ButtonToolbar, Button, Modal, Form} from 'react-bootstrap';

function Example(props) {
    const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    return (
        <>
            <Button variant="primary" className={props.class} onClick={handleShow}>
                {props.name}
            </Button>

            <Modal show={show} onHide={handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>Q timex 1979 reissue</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form>
                        <Form.Row>
                            <Form.Group as={Col} controlId="formGridEmail">
                                <Form.Label>Email</Form.Label>
                                <Form.Control type="email" placeholder="Enter email" />
                            </Form.Group>
                        </Form.Row>
                        <Form.Group controlId="formGridAddress1">
                            <Form.Label>Address</Form.Label>
                            <Form.Control placeholder="1234 Main St" />
                        </Form.Group>

                        <Form.Group controlId="formGridAddress2">
                            <Form.Label>Address 2</Form.Label>
                            <Form.Control placeholder="Apartment, studio, or floor" />
                        </Form.Group>

                        <Form.Row>
                            <Form.Group as={Col} controlId="formGridCity">
                                <Form.Label>City</Form.Label>
                                <Form.Control />
                            </Form.Group>

                            <Form.Group as={Col} controlId="formGridState">
                                <Form.Label>State</Form.Label>
                                <Form.Control as="select">
                                    <option>Choose...</option>
                                    <option>...</option>
                                </Form.Control>
                            </Form.Group>

                            <Form.Group as={Col} controlId="formGridZip">
                                <Form.Label>Zip</Form.Label>
                                <Form.Control />
                            </Form.Group>
                        </Form.Row>

                        <Form.Group id="formGridCheckbox">
                            <Form.Check type="checkbox" label="Check me out" />
                        </Form.Group>
                    </Form>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="primary" type="submit">
                        Submit
                    </Button>
                    <Button variant="secondary" onClick={handleClose}>
                        Close
                    </Button>
                </Modal.Footer>
            </Modal>
        </>
    );
}


class LastText extends React.Component {
    render() {
        return (
            <React.Fragment>
                <Container>
                    <Row className="my-5">
                        <Col xl={12} className="my-5">
                            <p className="text-left">Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                Asperiores aut dignissimos doloribus ducimus, ex, inventore iure nam nisi numquam
                                officiis quaerat sed sunt voluptatibus. Aut consequuntur cupiditate fugit hic officia.
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid eos, ex explicabo
                                illum iusto libero maxime neque nulla numquam, possimus praesentium quam sunt vitae.
                                Blanditiis dolor molestias nihil quod ratione.
                            </p>
                            <p className="text-left mt-3">Terms & Conditions:</p>
                          <ol className="text-left mt-3">
                              <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</li>
                              <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</li>
                              <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</li>
                              <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</li>
                              <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</li>
                              <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</li>
                          </ol>
                            <p className="text-left mt-4">Please reade carefully terms and conditions. see you at the event and good luck</p>
                            <ButtonToolbar className="justify-content-center mt-5">
                                <Example name="Register" class="buttonPrimary" />
                            </ButtonToolbar>
                        </Col>
                    </Row>
                </Container>
            </React.Fragment>
        )
    }
}


export default LastText;