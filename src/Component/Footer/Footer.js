import React from 'react';
import {Container, Row, Col, InputGroup, FormControl, Carousel, Button} from 'react-bootstrap';
import './Footer.css';
import 'boxicons'

function Footer() {
    return (
        <div className="footer">
            <Container fluid="true">
                <div className="mx-3">
                    <Row>
                        <Col lg={3}>
                            <ul className="list-unstyled text-uppercase my-5">
                                <li><a href="#">About</a></li>
                                <li><a href="#">Shipping Information</a></li>
                                <li><a href="#">Term & Conditions</a></li>
                                <li><a href="#">Faq</a></li>
                                <li><a href="#">Warranty & service</a></li>
                                <li><a href="#">Contact</a></li>
                                <li><a href="#">Store location</a></li>
                            </ul>

                            <div className="d-inline hello">
                                <div className="my-5">
                                    <a href="#">
                                        <box-icon type='logo' name='twitter' color={"black"}></box-icon>
                                    </a>
                                    <a href="#">
                                        <box-icon name='facebook' type='logo' color={"black"}></box-icon>
                                    </a>
                                    <a href="#">
                                        <box-icon name='instagram' type='logo' color={"black"}></box-icon>
                                    </a>
                                    <a href="#">
                                        <box-icon name='pinterest-alt' type='logo' color={"black"}></box-icon>
                                    </a>
                                </div>
                            </div>
                        </Col>
                        <Col lg={6}>
                            <div className="my-5">
                                <div className="d-inline">
                                    <InputGroup className="mb-2">
                                        <FormControl
                                            placeholder="Enter your Email Address"
                                            aria-label="Recipient's username"
                                            aria-describedby="basic-addon2"
                                        />
                                        <InputGroup.Append>
                                            <Button variant="outline-secondary" className="ml-3">subscribe</Button>
                                        </InputGroup.Append>
                                    </InputGroup>
                                    <p className={"mb-0"}>
                                        <small>And Exclusive promotions</small>
                                    </p>
                                    <p>
                                        <small>Sign up for our newsletter and stay up to date with out lastest releases,
                                            event
                                        </small>
                                    </p>
                                    <p className="my-5">
                                        Customer Sevice
                                    </p>
                                    <p className="mb-4 float-left">
                                        Get in touch/+62 8888 88888
                                        (Mon-Sat 10am-8pm CET)
                                    </p>
                                    <div className="float-right mr-3">
                                        <img
                                            src='https://barcode.tec-it.com/barcode.ashx?data=This+is+a+QR+Code+by+TEC-IT&code=QRCode&multiplebarcodes=false&translate-esc=false&unit=Fit&dpi=96&imagetype=Gif&rotation=0&color=%23000000&bgcolor=%23ffffff&qunit=Mm&quiet=0&eclevel=L'
                                            alt='Barcode Generator TEC-IT'/>
                                        <div className="text-center font-weight-bold">Line</div>
                                    </div>
                                </div>
                            </div>
                        </Col>
                        <Col lg={3}>
                            <p className="mt-5 font-weight-light">
                                Cicilan 0%
                            </p>

                            <Carousel className="mb-5" controls={false}>
                                <Carousel.Item>
                                  <span className="d-inline-block logos">
                                    <img src="//logo.clearbit.com/bca.co.id" alt="Bank"/>
                                    <img src="//logo.clearbit.com/uobgroup.com" alt="Bank" className={"ml-4"}/>
                                    <img src="//logo.clearbit.com/permatabank.com" alt="Bank" className={"ml-4"}/>
                                  </span>
                                    <span className="d-inline-block logos mt-4">
                                    <img src="//logo.clearbit.com/mandirikartukredit.com" alt="Bank"/>
                                    <img src="//logo.clearbit.com/cimbclicks.com.my" alt="Bank" className={"ml-4"}/>
                                    <img src="//logo.clearbit.com/hsbc.com" alt="Bank" className={"ml-4"}/>
                            </span>
                                    <span className="d-inline-block logos mt-4">
                                    <img src="//logo.clearbit.com/sc.com" alt="Bank"/>
                                    <img src="//logo.clearbit.com/bni.co.id" alt="Bank" className={"ml-4"}/>
                                    <img src="//logo.clearbit.com/anz.com.au" alt="Bank" className={"ml-4"}/>
                            </span>
                                </Carousel.Item>
                                <Carousel.Item>
                                    <span className="d-inline-block logos">
                                    <img alt="Bank" src="//logo.clearbit.com/bca.co.id"/>
                                    <img alt="Bank" src="//logo.clearbit.com/uobgroup.com" className={"ml-4"}/>
                                    <img alt="Bank" src="//logo.clearbit.com/permatabank.com" className={"ml-4"}/>
                            </span>
                                    <span className="d-inline-block logos mt-4">
                                    <img alt="Bank" src="//logo.clearbit.com/mandirikartukredit.com"/>
                                    <img alt="Bank" src="//logo.clearbit.com/cimbclicks.com.my" className={"ml-4"}/>
                                    <img alt="Bank" src="//logo.clearbit.com/hsbc.com" className={"ml-4"}/>
                            </span>
                                    <span className="d-inline-block logos mt-4">
                                    <img alt="Bank" src="//logo.clearbit.com/sc.com"/>
                                    <img alt="Bank" src="//logo.clearbit.com/bni.co.id" className={"ml-4"}/>
                                    <img alt="Bank" src="//logo.clearbit.com/anz.com.au" className={"ml-4"}/>
                            </span>
                                </Carousel.Item>
                                <Carousel.Item>
                                   <span className="d-inline-block logos">
                                    <img alt="Bank" src="//logo.clearbit.com/bca.co.id"/>
                                    <img alt="Bank" src="//logo.clearbit.com/uobgroup.com" className={"ml-4"}/>
                                    <img alt="Bank" src="//logo.clearbit.com/permatabank.com" className={"ml-4"}/>
                            </span>
                                    <span className="d-inline-block logos mt-4">
                                    <img alt="Bank" src="//logo.clearbit.com/mandirikartukredit.com"/>
                                    <img alt="Bank" src="//logo.clearbit.com/cimbclicks.com.my" className={"ml-4"}/>
                                    <img alt="Bank" src="//logo.clearbit.com/hsbc.com" className={"ml-4"}/>
                            </span>
                                    <span className="d-inline-block logos mt-4">
                                    <img alt="Bank" src="//logo.clearbit.com/sc.com"/>
                                    <img alt="Bank" src="//logo.clearbit.com/bni.co.id" className={"ml-4"}/>
                                    <img alt="Bank" src="//logo.clearbit.com/anz.com.au" className={"ml-4"}/>
                            </span>
                                </Carousel.Item>
                            </Carousel>
                        </Col>
                    </Row>
                </div>
            </Container>
        </div>

    );
}

export default Footer;