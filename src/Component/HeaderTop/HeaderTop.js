import React,{Fragment} from 'react';
import {Container, Row, Col} from 'react-bootstrap';
import './HeaderTop.css';
import 'boxicons';

class ScrollButton extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            intervalId: 0
        };
    }

    scrollStep() {
        if (window.pageYOffset === 0) {
            clearInterval(this.state.intervalId);
        }
        window.scroll(0, window.pageYOffset - this.props.scrollStepInPx);
    }

    scrollToTop() {
        let intervalId = setInterval(this.scrollStep.bind(this), this.props.delayInMs);
        this.setState({ intervalId: intervalId });
    }

    render () {
        return <button title='Back to top' className='scroll'
                       onClick={ () => { this.scrollToTop(); }}>
            <span><box-icon name='chevron-up' color="white"></box-icon></span>
        </button>;
    }
}

class HeaderTop extends React.Component{
    render(){
        return(
            <Fragment>
                <div className="HeaderTop">
                    <Container>
                        <Row>
                            <Col lg={12}>
                                <div className="logo-left">
                                    <div className="logo">
                                        <box-icon name='dropbox' type='logo' color='#ffffff' size={"md"} ></box-icon>
                                        <p>Free Shipping</p>
                                    </div>
                                    <div className="logo">
                                        <box-icon name='watch' type='solid' color='#ffffff' size={"md"} ></box-icon>
                                        <p>lifetime battery</p>
                                    </div>
                                    <div className="logo">
                                        <box-icon name='credit-card' type='solid' color='#ffffff' size={"md"}></box-icon>
                                        <p>0% installment</p>
                                    </div>
                                </div>
                                <div className="float-right logo-right">
                                    online exclusive-all straps 30%
                                    <span className="font-weight-bold ml-2">Shop Now</span>
                                </div>
                            </Col>
                        </Row>
                    </Container>
                </div>
                <ScrollButton scrollStepInPx="50" delayInMs="16.66"/>
            </Fragment>
        )
    }
}


export default HeaderTop