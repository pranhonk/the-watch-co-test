import React, {Fragment} from "react";
import {Container, Navbar, Nav} from "react-bootstrap";
import "./Header.css";
import Logo from "../Assets/Image/logo-the-watch-co.png";
import "boxicons";
import {connect} from "react-redux";
import {Dropdown, Form, Row, Col, Button} from 'react-bootstrap';
import Proceed from './Proceed';
import NumberFormat from 'react-number-format';

class Header extends React.Component {
    render() {
        return (
            <Fragment>
                <Navbar bg="light" expand="lg" className="navbar-header">
                    <Container>
                        <Navbar.Brand href="#home">
                            <img src={Logo} alt="The Watch Co"/>
                        </Navbar.Brand>
                        <Navbar.Toggle aria-controls="basic-navbar-nav"/>
                        <Navbar.Collapse className="justify-content-center menu-mobile">
                            <Nav>
                                <Nav.Link href="#home">Jam Tangan</Nav.Link>
                                <Nav.Link href="#link">Strap</Nav.Link>
                                <Nav.Link href="#link">Aksesoris</Nav.Link>
                                <Nav.Link href="#link">Perhiasan</Nav.Link>
                                <Nav.Link href="#link">Brand</Nav.Link>
                                <Nav.Link href="#link">Jurnal</Nav.Link>
                                <Nav.Link href="#link">Diskon</Nav.Link>
                            </Nav>
                        </Navbar.Collapse>
                        <Navbar.Collapse className="justify-content-end menu-mobile">
                            <box-icon type="solid" name="user" class={"mr-2"}/>
                            <Dropdown>
                                <Dropdown.Toggle id="dropdown-basic">
                                    <box-icon name="cart" type="solid" className={"mr-2"}/>
                                    <div className={this.props.addToCart === true ? 'number d-block' : 'number'}>
                                        {this.props.total}
                                    </div>
                                </Dropdown.Toggle>
                                <Dropdown.Menu>
                                    <Dropdown.Item>
                                        <Form>
                                            <Form.Group as={Row} controlId="formPlaintextEmail">
                                                <Form.Label column sm="2">
                                                    Total
                                                </Form.Label>
                                                <Col xs={12}>
                                                    <Form.Control plaintext readOnly defaultValue={this.props.total}
                                                                  Value={this.props.total} className="text-center"/>
                                                </Col>
                                            </Form.Group>
                                        </Form>
                                        <Form>
                                            <Form.Group as={Row} controlId="formPlaintextEmail">
                                                <Form.Label column sm="2">
                                                    Harga
                                                </Form.Label>
                                                <Col xs={12}>
                                                    <NumberFormat value={this.props.price} className="text-success font-weight-bold"
                                                                  displayType={'Number'} thousandSeparator={true} prefix={'IDR'}/><br/>
                                                </Col>
                                            </Form.Group>
                                        </Form>
                                    </Dropdown.Item>
                                    <div className="text-center">
                                        <Proceed />
                                        <Button variant="success" className="w-75" onClick={this.props.Open} disabled={this.props.total === 0}>Check Out</Button>
                                    </div>
                                </Dropdown.Menu>
                            </Dropdown>
                            <box-icon name="search-alt-2"/>
                        </Navbar.Collapse>
                    </Container>
                </Navbar>
                <Proceed />
            </Fragment>
        );
    }
}

const mapStateToProps = state => {
    return {
        total: state.value,
        price: state.price,
        show: state.show,
        addToCart: state.addToCart
    };
};

const mapDispatchToProps = (dispatch)=>{
    return{
        Open: ()=>dispatch({type: "Open"}),
        Close: ()=>dispatch({type: "Close"})
    }
};
export default connect(mapStateToProps, mapDispatchToProps)(Header);
