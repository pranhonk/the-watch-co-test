import React from 'react';
import {Modal, Button} from 'react-bootstrap';
import {connect} from 'react-redux';
import watch from '../Assets/Image/P7.png';
import NumberFormat from 'react-number-format';

class Proceed extends React.Component {
    render() {
        return (
            <>
                <Modal show={this.props.show} onHide={this.props.Close}>
                    <Modal.Header closeButton>
                        <Modal.Title>Checkout</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <div className="d-block">
                            <img src={watch} alt="Q Timex Watch" className="w-25"/>
                            <h5 className="text-uppercase mt-4">Q timex 1979 reissue</h5>
                            <div className="d-block float-left my-3">
                                <Button onClick={this.props.Minus} variant="dark" className="d-inline mr-2">-</Button>
                                <input type="number" value={this.props.total} className="w-25"/>
                                <Button onClick={this.props.Plus} variant="dark" className="d-inline ml-2">+</Button>
                            </div>
                            <NumberFormat value={this.props.price} className="w-50 float-right text-success font-weight-bold"
                                          displayType={'Number'} thousandSeparator={true} prefix={'IDR'}/>
                        </div>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary">
                            Input Shipping Address
                        </Button>
                    </Modal.Footer>
                </Modal>
            </>
        );
    }
}

const mapStateToProps = state => {
    return {
        total: state.value,
        price: state.price,
        show: state.show
    };
};

const mapDispatchToProps = dispacth => {
    return {
        Open: () => dispacth({type: "Open"}),
        Close: () => dispacth({type: "Close"}),
        Plus: () => dispacth({type: 'Plus'}),
        Minus: () => dispacth({type: 'Minus'})
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Proceed);